/**
 * 
 */
package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import listepokemon.TriListe;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TestTriListe {

	ArrayList<String> liste;

	/**
	 * Pr�paration avant chaque test
	 */
	@Before
	public void setup() {
		liste = new ArrayList<String>();

		liste.add("a");
		liste.add("z");
		liste.add("aa");
		liste.add("az");
		liste.add("b");
	}
	
	@Test
	public void constructeurVide() {
		TriListe t = new TriListe();
	}

	/**
	 * Test de la m�thode de tri d'une listee
	 */
	@Test
	public void triList() {

		TriListe.trier(liste);

		assertEquals("L'indice 0 de la listee devrait �tre a", "a", liste.get(0));
		assertEquals("L'indice 1 de la listee devrait �tre aa", "aa", liste.get(1));
		assertEquals("L'indice 2 de la listee devrait �tre az", "az", liste.get(2));
		assertEquals("L'indice 3 de la listee devrait �tre b", "b", liste.get(3));
		assertEquals("L'indice 4 de la listee devrait �tre z", "z", liste.get(4));
	}

}
