/**
 * 
 */
package listepokemon;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TriListe {

	/**
	 * Permet de trier une liste de String
	 * 
	 * @param list Liste � trier
	 */
	public static void trier(ArrayList<String> list) {
		for (int i = list.size() - 1; i > 0; --i) {
			boolean triee = true;
			for (int j = 0; j < i; ++j) {

				if (list.get(j + 1).compareTo(list.get(j)) < 0) {
					Collections.swap(list, j, j + 1);
					triee = false;
				}
			}

			if (triee)
				break;

		}
	}
}
